﻿using System.IO;
using Xamarin.Forms;
using lab14_crudPersona.Droid.Implementations;
using lab14_crudPersona.Interfaces;

[assembly: Dependency(typeof(ConfigDataBase))]
namespace lab14_crudPersona.Droid.Implementations
{
    public class ConfigDataBase : IConfigDataBase
    {
        public string GetFullPath(string databaseFileName)
        {
            return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), databaseFileName);
        }
    }
}