﻿using System;
using System.IO;
using Xamarin.Forms;
using lab14_crudPersona.Interfaces;
using lab14_crudPersona.iOS.Implementations;

[assembly: Dependency(typeof(ConfigDataBase))]
namespace lab14_crudPersona.iOS.Implementations
{
    public class ConfigDataBase : IConfigDataBase
    {
        public string GetFullPath(string databaseFileName)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", databaseFileName);
        }
    }
}