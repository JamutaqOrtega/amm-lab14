﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using lab14_crudPersona.Models;
using lab14_crudPersona.Services;

namespace lab14_crudPersona.ViewModels
{
    public class PersonsViewModel : BaseViewModel
    {
        #region Services
        private readonly DBDataAccess<Person> dataServicePersons;
        #endregion Services

        #region Attributes
        private ObservableCollection<Person> persons;
        private string fullName;
        private DateTime dateOfBirth;
        private string dni;
        private bool sex;
        #endregion Attributes

        #region Properties
        public ObservableCollection<Person> Persons
        {
            get { return this.persons; }
            set { SetValue(ref this.persons, value); }
        }

        public string FullName
        {
            get { return this.fullName; }
            set { SetValue(ref this.fullName, value); }
        }

        public DateTime DateOfBirth
        {
            get { return this.dateOfBirth; }
            set { SetValue(ref this.dateOfBirth, value); }
        }

        public string Dni
        {
            get { return this.dni; }
            set { SetValue(ref this.dni, value); }
        }

        public bool Sex
        {
            get { return this.sex; }
            set { SetValue(ref this.sex, value); }
        }
        #endregion Properties

        #region Constructor
        public PersonsViewModel()
        {
            this.dataServicePersons = new DBDataAccess<Person>();

            this.LoadPersons();

            //this.DateOfBirth = DateTime.Now;
        }
        #endregion Constructor

        #region Commands
        public ICommand CreateCommand
        {
            get
            {
                return new Command(async () =>
                {
                    var newPerson = new Person()
                    {
                        FullName = this.FullName,
                        DateOfBirth = this.DateOfBirth,
                        Dni = this.Dni,
                        Sex = this.Sex,
                    };

                    if (newPerson != null)
                    {
                        if (this.dataServicePersons.Create(newPerson))
                        {
                            await Application.Current.MainPage.DisplayAlert("Operación Exitosa",
                                                                            $"Persona: {this.FullName} " +
                                                                            $"registrada correctamente en la base de datos",
                                                                            "Ok");

                            this.FullName = string.Empty;
                            this.DateOfBirth = DateTime.Now;
                            this.Dni = string.Empty;
                            this.Sex = false;
                        }

                        else
                            await Application.Current.MainPage.DisplayAlert("Operación Fallida",
                                                                            $"Error al registrar a la persona en la base de datos",
                                                                            "Ok");
                    }
                });
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return new Command<string>(
                    execute: async (string arg) =>
                    {
                        string person_id = arg;
                        int.TryParse(person_id, out int id);

                        try
                        {
                            this.dataServicePersons.Delete(id);

                            await Application.Current.MainPage.DisplayAlert("Operación Exitosa",
                                                                            $"Se borró la persona con el ID: {person_id}",
                                                                            "Ok");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            await Application.Current.MainPage.DisplayAlert("Operación Fallida",
                                                                            $"Error al eliminar el registro",
                                                                            "Ok");
                        }
                    }
                );
            }
        }
        #endregion Commands

        #region Methods
        private void LoadPersons()
        {
            var personsDB = this.dataServicePersons.Get().ToList() as List<Person>;
            this.Persons = new ObservableCollection<Person>(personsDB);
        }
        #endregion Methods
    }
}
