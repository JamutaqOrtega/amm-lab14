﻿using System;

namespace lab14_crudPersona.Models
{
    public class Person
    {
        public int PersonID { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Dni { get; set; }
        public bool Sex { get; set; } //true:femenino, false:masculino
    }
}
