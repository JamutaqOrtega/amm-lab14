﻿using Microsoft.EntityFrameworkCore;
using lab14_crudPersona.Models;

namespace lab14_crudPersona.DataContext
{
    public class AppDbContext : DbContext
    {
        readonly string DbPath = string.Empty;

        public AppDbContext(string dbPath)
        {
            this.DbPath = dbPath;
        }

        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={DbPath}");
        }
    }
}
