﻿using Xamarin.Forms;
using lab14_crudPersona.DataContext;
using lab14_crudPersona.Interfaces;
using lab14_crudPersona.Views;

namespace lab14_crudPersona
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            GetContext().Database.EnsureCreated();
            MainPage = new NavigationPage(new Views.HomePage());
        }

        /**
         * Método para obtener el contexto cuando se inicia la aplicación
         */
        public static AppDbContext GetContext()
        {
            string DbPath = DependencyService.Get<IConfigDataBase>().GetFullPath("efCore.db");

            return new AppDbContext(DbPath);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
