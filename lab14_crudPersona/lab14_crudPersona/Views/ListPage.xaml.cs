﻿using lab14_crudPersona.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace lab14_crudPersona.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListPage : ContentPage
    {
        public ListPage()
        {
            InitializeComponent();
            BindingContext = new PersonsViewModel();
        }

        private void Delete(object sender, EventArgs e)
        {
            //obtiene el CommandParameter
            Button _myButton = (Button)sender;
            string value = _myButton.CommandParameter.ToString();

            //llama al comando DeleteCommand
            var viewModel = (PersonsViewModel)BindingContext;
            if (viewModel.DeleteCommand.CanExecute(null))
                viewModel.DeleteCommand.Execute(value);
        }
    }
}